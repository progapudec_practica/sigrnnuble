from cpoly.models import ShapePoly
from cpoint.models import ShapePoint
from django.utils.safestring import mark_safe
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
import django_tables2 as tables
from django.db.models import Q, Subquery, QuerySet

class ShapePolyTable(tables.Table):
    options = tables.Column(verbose_name=_('Opciones'), orderable=False, empty_values=(),
                            attrs={
                                'td': {'class': 'text-center'},
                                'th': {"class": "text-center col-md-2"}})

    class Meta:
        model = ShapePoly
        template = 'django_tables2/bootstrap.html'
        exclude = ('geom', 'estado')
        sequence = ('tipo', 'superficie')

    def __init__(self, *args, **kwargs):
        super(ShapePolyTable, self).__init__(*args, **kwargs)
        self.delete_text = _('Mostrar SIG')

class ShapePointTable(tables.Table):
    options = tables.Column(verbose_name=_('Opciones'), orderable=False, empty_values=(),
                            attrs={
                                'td': {'class': 'text-center'},
                                'th': {"class": "text-center col-md-2"}})

    class Meta:
        model = ShapePoint
        template = 'django_tables2/bootstrap.html'
        exclude = ('geom', 'estado')
        sequence = ('tipo', 'superficie')

    def __init__(self, *args, **kwargs):
        super(ShapePointTable, self).__init__(*args, **kwargs)
        self.delete_text = _('Mostrar SIG')