from django.contrib import admin
from cpoly.models import ShapePoly
from leaflet.admin import LeafletGeoAdmin
# Register your models here.
class AdminShapePoly(LeafletGeoAdmin):
	list_display = ['id_poly','snaspe','tipo','superficie','descripcio','nom_regi','nom_prov','nom_comuna','horario_in','horario_ve','shape_leng','shape_area']
	list_filter = ['id_poly']
	search_fields = ['id_poly']

admin.site.register(ShapePoly, AdminShapePoly)