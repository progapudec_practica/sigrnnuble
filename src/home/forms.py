import os
import os.path
import shutil
import tempfile
import traceback
import zipfile
from django.conf import settings
from django import forms
from zipfile import ZipFile, BadZipfile
from django.core.validators import FileExtensionValidator
from os import system, chdir
from osgeo import ogr
from sys import exit
import sys
from django.http import FileResponse
from cpoly.models import ShapePoly

class ShapePolyForm(forms.Form):
    archivo_zip_shape = forms.FileField('Archivos recursos poligono ".shp", ".shx", ".dbf", ".prj"')

    def process_file(self):
        # Ruta donde se encuentra el fichero
        zip_filename = self.cleaned_data['archivo_zip_shape']

        # Lugar donde se alojarán los ficheros descomprimidos
        dirname = settings.MEDIA_ROOT2 + '/Shapes/poligonos'

        zip = ZipFile(zip_filename)

        lista_ficheros = []

        # Recorremos todos los ficheros que contiene el zip
        shapefile_name = None
        for filename in zip.namelist():

            if filename.endswith('.shp'):
                shapefile_name = filename

            ruta_total = os.path.join(dirname, filename)

            # Si es un directorio, lo creamos
            if filename.endswith('/'):
                try:  # Don't try to create a directory if exists
                    os.mkdir(ruta_total)
                except:
                    pass
            # Si es un fichero, lo escribimos
            else:
                outfile = open(ruta_total, 'wb')
                outfile.write(zip.read(filename))
                outfile.close()
                lista_ficheros.append(ruta_total)
        zip.close()
        # os.unlink(zip_filename)

        return shapefile_name

class ShapePointForm(forms.Form):
    archivo_zip_shape = forms.FileField('Archivos recursos punto ".shp", ".shx", ".dbf", ".prj"')

    def process_file(self):
        # Ruta donde se encuentra el fichero
        zip_filename = self.cleaned_data['archivo_zip_shape']

        # Lugar donde se alojarán los ficheros descomprimidos
        dirname = settings.MEDIA_ROOT2 + '/Shapes/puntos'

        zip = ZipFile(zip_filename)

        lista_ficheros = []

        # Recorremos todos los ficheros que contiene el zip
        shapefile_name = None
        for filename in zip.namelist():

            if filename.endswith('.shp'):
                shapefile_name = filename

            ruta_total = os.path.join(dirname, filename)

            # Si es un directorio, lo creamos
            if filename.endswith('/'):
                try:  # Don't try to create a directory if exists
                    os.mkdir(ruta_total)
                except:
                    pass
            # Si es un fichero, lo escribimos
            else:
                outfile = open(ruta_total, 'wb')
                outfile.write(zip.read(filename))
                outfile.close()
                lista_ficheros.append(ruta_total)
        zip.close()
        # os.unlink(zip_filename)

        return shapefile_name