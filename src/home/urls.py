from django.conf.urls import include,url
from home.views import PaginaPrincipal, ShapePolyList, ShapePointList
from home.shapes import ShapePolyCargar, ShapePointCargar

urlpatterns = [
    url(r'^$', PaginaPrincipal.as_view(), name='inicio'),
	url(r'^home/shapes/poly/$', ShapePolyCargar.as_view(), name='shape_poligono_cargar'),
	url(r'^home/shapes/poly/cargar/$', ShapePolyCargar.upload_shape_polygon, name='upload_shape_polygon'),
	url(r'^home/shapes/poly/listar$', ShapePolyList.as_view(), name='shape_poligono_listar'),
	url(r'^home/shapes/point/$', ShapePointCargar.as_view(), name='shape_punto_cargar'),
	url(r'^home/shapes/point/cargar$', ShapePointCargar.upload_shape_point, name='upload_shape_point'),
	url(r'^home/shapes/point/listar$', ShapePointList.as_view(), name='shape_point_listar'),
]