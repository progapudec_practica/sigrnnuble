from django.db import models
from django.contrib.gis.db import models

# Create your models here.
class ShapePoint(models.Model):
    id_point = models.BigIntegerField()
    snaspe = models.CharField(max_length=254)
    tipo = models.CharField(max_length=254)
    superficie = models.FloatField()
    descripcio = models.CharField(max_length=254)
    nom_regi = models.CharField(max_length=254)
    nom_comuna = models.CharField(max_length=254)
    nom_prov = models.CharField(max_length=254)
    shape_leng = models.FloatField()
    shape_area = models.FloatField()
    horario_in = models.CharField(max_length=254)
    horario_ve = models.CharField(max_length=254)
    geom = models.PointField(srid=32719)

    def __str__(self):
    	return str("ID shape punto: " + str(self.id_point))

    class Meta:
    	verbose_name = "Recurso punto"
    	verbose_name_plural = "Recursos punto"