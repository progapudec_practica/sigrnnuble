from django.db import models

# Create your models here.
class Shapefile_upload(models.Model):
    filename = models.CharField(max_length=200, verbose_name='Nombre de Archivo')
    date_uploaded = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de carga')
    archivo = models.CharField(max_length=200, verbose_name='Nombre del Archivo', null=True)

    def __str__(self):
        return self.filename

    class Meta:
        verbose_name = "Shape Upload"
        verbose_name_plural = "Shapes Uploaded"
        permissions = (
            ("view_Shapefile_upload", "Puede ver Shapes"),
            ("import_Shapefile_upload", "Puede importar Shapes"),
            ("export_Shapefile_upload", "Puede exportar Shapes")
        )
