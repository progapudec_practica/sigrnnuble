from django.contrib import admin
from cpoint.models import ShapePoint
from leaflet.admin import LeafletGeoAdmin
# Register your models here.
class AdminShapePoint(LeafletGeoAdmin):
	list_display = ['id_point','snaspe','tipo','superficie','descripcio','nom_regi','nom_comuna','nom_prov','shape_area','horario_ve','horario_in']
	list_filter = ['id_point']
	search_fields = ['id_point']
admin.site.register(ShapePoint, AdminShapePoint)