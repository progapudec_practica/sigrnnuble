from django.db import models
from django.contrib.gis.db import models

# Create your models here.

class ShapePoly(models.Model):
    id_poly = models.BigIntegerField()
    snaspe = models.CharField(max_length=254)
    tipo = models.CharField(max_length=254)
    superficie = models.FloatField()
    descripcio = models.CharField(max_length=254)
    nom_regi = models.CharField(max_length=254)
    nom_prov = models.CharField(max_length=254)
    nom_comuna = models.CharField(max_length=254)
    horario_in = models.CharField(max_length=254)
    horario_ve = models.CharField(max_length=254)
    shape_leng = models.FloatField()
    shape_area = models.FloatField()
    geom = models.MultiPolygonField(srid=4326)

    def __str__(self):
        return str("ID polígono: " + str(self.id_poly))

    class Meta:
        verbose_name = "Recurso polígono"
        verbose_name_plural = "Recursos polígono"