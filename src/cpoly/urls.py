from django.conf.urls import include,url
from cpoly.views import shape_poly_datasets

urlpatterns = [
    url(r'^poligono_data/$', shape_poly_datasets, name='poligono'),
]