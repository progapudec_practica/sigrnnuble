from django.shortcuts import render
from django.views.generic import TemplateView
from cpoly.models import ShapePoly
from cpoint.models import ShapePoint
from home.tables import ShapePolyTable, ShapePointTable
from django_tables2 import SingleTableView

# Create your views here.
class PaginaPrincipal(TemplateView):
	template_name = 'index.html'

class ShapePolyList(SingleTableView):
    required_permissions = ('home.view_ShapePoly',)
    template_name = 'sig/shape_poligono_listar.html'
    model = ShapePoly
    table_class = ShapePolyTable
    paginate_by = 20

    def get_queryset(self):
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = Q(nombre__icontains=search) | \
                 Q(rol__icontains=search)

            return ShapePoly.objects.filter(qs).order_by('id')

        return ShapePoly.objects.get_queryset().order_by('id')

    def get_context_data(self, **kwargs):
        context = super(ShapePolyList, self).get_context_data(**kwargs)
        context['q'] = self.request.GET.get('q', '')
        return context

class ShapePointList(SingleTableView):
    required_permissions = ('home.view_ShapePoly',)
    template_name = 'sig/shape_punto_listar.html'
    model = ShapePoint
    table_class = ShapePointTable
    paginate_by = 20

    def get_queryset(self):
        if self.request.GET.get('q', False):
            search = self.request.GET['q']
            qs = Q(nombre__icontains=search) | \
                 Q(rol__icontains=search)

            return ShapePoint.objects.filter(qs).order_by('id')

        return ShapePoint.objects.get_queryset().order_by('id')

    def get_context_data(self, **kwargs):
        context = super(ShapePointList, self).get_context_data(**kwargs)
        context['q'] = self.request.GET.get('q', '')
        return context