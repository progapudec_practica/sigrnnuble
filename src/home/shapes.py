from .forms import *
from cpoly.models import ShapePoly
from cpoint.models import ShapePoint
from home.models import Shapefile_upload
from django.views.generic import FormView
from django_tables2 import SingleTableView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
#from users.mixins import PermissionsRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
import os.path
from django.db.models import Q
from home.load_layer import *
from django.contrib.messages import constants as messages
from django.contrib import messages
import datetime
from osgeo import ogr
import zipfile
import io

MESSAGE_TAGS = {
    messages.SUCCESS: '',
    messages.ERROR: ''
}


def get_date_hour():
    ahora = datetime.datetime.now()
    return str(ahora)

class ShapePolyCargar(SuccessMessageMixin, LoginRequiredMixin, FormView):
    #required_permissions = ('home.import_Shapefile_upload',)
    form_class = ShapePolyForm
    template_name = 'sig/shape_poligono_cargar.html'
    success_message = 'Archivo borrado exitosamente'
    error_message = 'El archivo no es un .zip valido'
    success_url = reverse_lazy('home:shape_poligono_cargar')

    def get_queryset(self):
        user = self.request.user.username
        return user

    def upload_shape_polygon(request):
        def clean_file(shapefile):
            fd, fname = tempfile.mkstemp(suffix=".zip")
            os.close(fd)
            f = open(fname, "wb")

            for chunk in shapefile.chunks():
                f.write(chunk)
            f.close()

            if not zipfile.is_zipfile(fname):
                os.remove(fname)
                return "¡ATENCIÓN! el archivo que intenta cargar no corresponde a la extensión .zip, porfavor verifique que los archivos necesarios para cargar el shape (.shp, .shx, .dbf, .prj) estén comprimidos en un .zip."

            zip = zipfile.ZipFile(fname)
            required_suffixes = [".shp", ".shx", ".dbf", ".prj"]
            has_suffix = {}
            for suffix in required_suffixes:
                has_suffix[suffix] = False

            for info in zip.infolist():
                suffix = os.path.splitext(info.filename)[1].lower()
                if suffix in required_suffixes:
                    has_suffix[suffix] = True

            for suffix in required_suffixes:
                if not has_suffix[suffix]:
                    zip.close()
                    os.remove(fname)
                    return "¡ATENCIÓN! el archivo que intenta cargar no contiene un archivo requerido de extensión " + suffix + ", porfavor verifique que los 4 archivos necesarios para cargar el shape (.shp, .shx, .dbf, .prj) se encuentren dentro del .zip que intenta cargar."

            shapefile_name = None
            dir_name = tempfile.mkdtemp()
            for info in zip.infolist():
                if info.filename.endswith(".shp"):
                    shapefile_name = info.filename

                dst_file = os.path.join(dir_name, info.filename)
                f = open(dst_file, "wb")
                f.write(zip.read(info.filename))
                f.close()
            zip.close()

            try:
                datasource = ogr.Open(os.path.join(dir_name, shapefile_name))
                layer = datasource.GetLayer(0)
                shapefile_ok = True
            except:
                traceback.print_exc()
                shapefile_ok = False

            if not shapefile_ok:
                os.remove(fname)
                shutil.rmtree(dir_name)
                return "***El archivo.shp No es un archivo shape valido."

            try:
                shapepoly_shp = os.path.abspath(os.path.join(dir_name, shapefile_name))
                lm = LayerMapping(ShapePoly, shapepoly_shp, shapepoly_mapping, transform=False, encoding='iso-8859-1')
                if lm.mapping == shapepoly_mapping:
                    shapefile_ok = True
            except:
                traceback.print_exc()
                shapefile_ok = False

            if not shapefile_ok:
                os.remove(fname)
                shutil.rmtree(dir_name)
                return "¡ATENCIÓN! el archivo que intenta cargar no cuenta con la estructura requerida para realizar la operación, verifique que la estructura del shape cumpla con la estructura que se solicita y vuelva a intentarlo."

        if request.method == 'POST':
            errMsg = None  # initially.
            form = ShapePolyForm(request.POST, request.FILES)
            # Si el formulario es válido, proceso el fichero
            if form.is_valid():
                shapefile = request.FILES['archivo_zip_shape']
                title = request.FILES['archivo_zip_shape'].name
                errMsg = clean_file(shapefile)
                if errMsg == None:
                    form.process_file()
                    listado = str(form.process_file())

                    def run_shapepoly(verbose=True):
                        shapepoly_shp = os.path.abspath(os.path.join(os.path.dirname(__file__), settings.MEDIA_ROOT2 + '/Shapes/poligonos/' + str(listado)))
                        lm = LayerMapping(ShapePoly, shapepoly_shp, shapepoly_mapping , transform=False, encoding='iso-8859-1')
                        delete_everything(delete_everything)
                        shapefile = Shapefile_upload(filename=listado, archivo=zip_dir)
                        shapefile.save()
                        lm.save(strict=True, verbose=verbose)

                    def delete_everything(self):
                        ShapePoly.objects.all().delete()

                    zip_compress = zipfile.ZipFile(settings.MEDIA_ROOT2 + '/Shapes/poligonos/Backup_' + listado + '_' + get_date_hour() + '.zip', 'w')

                    for folder, subfolders, files in os.walk(settings.MEDIA_ROOT2 + '/Shapes/poligonos'):
                        for file in files:
                            if file.endswith('.shp'):
                                zip_compress.write(os.path.join(folder, file), file, compress_type=zipfile.ZIP_DEFLATED)
                            if file.endswith('.shx'):
                                zip_compress.write(os.path.join(folder, file), file, compress_type=zipfile.ZIP_DEFLATED)
                            if file.endswith('.dbf'):
                                zip_compress.write(os.path.join(folder, file), file, compress_type=zipfile.ZIP_DEFLATED)
                            if file.endswith('.prj'):
                                zip_compress.write(os.path.join(folder, file), file, compress_type=zipfile.ZIP_DEFLATED)

                    zip_compress.close()
                    zip_dir = zip_compress.filename
                    run_shapepoly()
                    messages.success(request, 'Se ha cargado el archivo exitosamente.')
                    return HttpResponseRedirect('/home/shapes/poly/listar')
            return render(request, 'sig/shape_poligono_cargar.html', {'form': form,'errMsg': errMsg})

class ShapePointCargar(SuccessMessageMixin, LoginRequiredMixin, FormView):
    #required_permissions = ('home.import_Shapefile_upload',)
    form_class = ShapePointForm
    template_name = 'sig/shape_punto_cargar.html'
    success_message = 'Archivo borrado exitosamente'
    error_message = 'El archivo no es un .zip valido'
    success_url = reverse_lazy('home:shape_punto_cargar')

    def get_queryset(self):
        user = self.request.user.username
        return user

    def upload_shape_point(request):
        def clean_file(shapefile):
            fd, fname = tempfile.mkstemp(suffix=".zip")
            os.close(fd)
            f = open(fname, "wb")

            for chunk in shapefile.chunks():
                f.write(chunk)
            f.close()

            if not zipfile.is_zipfile(fname):
                os.remove(fname)
                return "¡ATENCIÓN! el archivo que intenta cargar no corresponde a la extensión .zip, porfavor verifique que los archivos necesarios para cargar el shape (.shp, .shx, .dbf, .prj) estén comprimidos en un .zip."

            zip = zipfile.ZipFile(fname)
            required_suffixes = [".shp", ".shx", ".dbf", ".prj"]
            has_suffix = {}
            for suffix in required_suffixes:
                has_suffix[suffix] = False

            for info in zip.infolist():
                suffix = os.path.splitext(info.filename)[1].lower()
                if suffix in required_suffixes:
                    has_suffix[suffix] = True

            for suffix in required_suffixes:
                if not has_suffix[suffix]:
                    zip.close()
                    os.remove(fname)
                    return "¡ATENCIÓN! el archivo que intenta cargar no contiene un archivo requerido de extensión " + suffix + ", porfavor verifique que los 4 archivos necesarios para cargar el shape (.shp, .shx, .dbf, .prj) se encuentren dentro del .zip que intenta cargar."

            shapefile_name = None
            dir_name = tempfile.mkdtemp()
            for info in zip.infolist():
                if info.filename.endswith(".shp"):
                    shapefile_name = info.filename

                dst_file = os.path.join(dir_name, info.filename)
                f = open(dst_file, "wb")
                f.write(zip.read(info.filename))
                f.close()
            zip.close()

            try:
                datasource = ogr.Open(os.path.join(dir_name, shapefile_name))
                layer = datasource.GetLayer(0)
                shapefile_ok = True
            except:
                traceback.print_exc()
                shapefile_ok = False

            if not shapefile_ok:
                os.remove(fname)
                shutil.rmtree(dir_name)
                return "***El archivo.shp No es un archivo shape valido."

            try:
                shapepoint_shp = os.path.abspath(os.path.join(dir_name, shapefile_name))
                lm = LayerMapping(ShapePoint, shapepoint_shp, shapepoint_mapping, transform=False, encoding='iso-8859-1')
                if lm.mapping == shapepoint_mapping:
                    shapefile_ok = True
            except:
                traceback.print_exc()
                shapefile_ok = False

            if not shapefile_ok:
                os.remove(fname)
                shutil.rmtree(dir_name)
                return "¡ATENCIÓN! el archivo que intenta cargar no cuenta con la estructura requerida para realizar la operación, verifique que la estructura del shape cumpla con la estructura que se solicita y vuelva a intentarlo."

        if request.method == 'POST':
            errMsg = None  # initially.
            form = ShapePointForm(request.POST, request.FILES)
            # Si el formulario es válido, proceso el fichero
            if form.is_valid():
                shapefile = request.FILES['archivo_zip_shape']
                title = request.FILES['archivo_zip_shape'].name
                errMsg = clean_file(shapefile)
                if errMsg == None:
                    form.process_file()
                    listado = str(form.process_file())

                    def run_shapepoint(verbose=True):
                        shapepoint_shp = os.path.abspath(os.path.join(os.path.dirname(__file__), settings.MEDIA_ROOT2 + '/Shapes/puntos/' + str(listado)))
                        lm = LayerMapping(ShapePoint, shapepoint_shp, shapepoint_mapping , transform=False, encoding='iso-8859-1')
                        delete_everything(delete_everything)
                        shapefile = Shapefile_upload(filename=listado, archivo=zip_dir)
                        shapefile.save()
                        lm.save(strict=True, verbose=verbose)

                    def delete_everything(self):
                        ShapePoint.objects.all().delete()

                    zip_compress = zipfile.ZipFile(settings.MEDIA_ROOT2 + '/Shapes/puntos/Backup_' + listado + '_' + get_date_hour() + '.zip', 'w')

                    for folder, subfolders, files in os.walk(settings.MEDIA_ROOT2 + '/Shapes/puntos'):
                        for file in files:
                            if file.endswith('.shp'):
                                zip_compress.write(os.path.join(folder, file), file, compress_type=zipfile.ZIP_DEFLATED)
                            if file.endswith('.shx'):
                                zip_compress.write(os.path.join(folder, file), file, compress_type=zipfile.ZIP_DEFLATED)
                            if file.endswith('.dbf'):
                                zip_compress.write(os.path.join(folder, file), file, compress_type=zipfile.ZIP_DEFLATED)
                            if file.endswith('.prj'):
                                zip_compress.write(os.path.join(folder, file), file, compress_type=zipfile.ZIP_DEFLATED)

                    zip_compress.close()
                    zip_dir = zip_compress.filename
                    run_shapepoint()
                    messages.success(request, 'Se ha cargado el archivo exitosamente.')
                    return HttpResponseRedirect('/home/shapes/point/listar')
            return render(request, 'sig/shape_punto_cargar.html', {'form': form,'errMsg': errMsg})

