from django.shortcuts import render
from django.views.generic import TemplateView
from django.core.serializers import serialize
from django.http import HttpResponse
from cpoly.models import ShapePoly
from cpoint.models import ShapePoint

# Create your views here.
def shape_poly_datasets(request):
    poligono = serialize('geojson', ShapePoly.objects.all())
    return HttpResponse(poligono, content_type='json')

def shape_point_datasets(request):
    punto = serialize('geojson', ShapePoint.objects.all())
    return HttpResponse(punto, content_type='json')