import os
from django.contrib.gis.utils import LayerMapping
from cpoly.models import ShapePoly
from django.conf import settings

shapepoly_mapping = {
    'id_poly' : 'id_poly',
    'snaspe' : 'Snaspe',
    'tipo' : 'Tipo',
    'superficie' : 'Superficie',
    'descripcio' : 'Descripcio',
    'nom_regi' : 'Nom_Regi',
    'nom_prov' : 'Nom_Prov',
    'nom_comuna' : 'Nom_Comuna',
    'horario_in' : 'Horario_In',
    'horario_ve' : 'Horario_Ve',
    'shape_leng' : 'Shape_leng',
    'shape_area' : 'Shape_area',
    'geom' : 'MULTIPOLYGON',
}

shapepoint_mapping = {
    'id_point' : 'id_point',
    'snaspe' : 'Snaspe',
    'tipo' : 'Tipo',
    'superficie' : 'Superficie',
    'descripcio' : 'Descripcio',
    'nom_regi' : 'Nom_Regi',
    'nom_comuna' : 'Nom_Comuna',
    'nom_prov' : 'Nom_Prov',
    'shape_leng' : 'Shape_leng',
    'shape_area' : 'Shape_area',
    'horario_in' : 'Horario_in',
    'horario_ve' : 'horario_ve',
    'geom' : 'POINT',
}
